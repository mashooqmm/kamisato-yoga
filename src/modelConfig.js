import * as tf from "@tensorflow/tfjs";
import "@mediapipe/pose";
import * as poseDetection from "@tensorflow-models/pose-detection";
import { state } from "./state";
import { addDerivedKeypoints, drawKeypoints, drawSkeleton } from "./canvas";
import { findAngle } from "./utils";
import { keypointsMapping } from "./mapping";
import { checkAllKepointsVisible } from "./utils";

let displayVid = true;
let detector = null;
const estimationConfig = { flipHorizontal: false };

// videoWidth = 640;
// videoHeight = 480;

// let video = document.getElementById("video");
// canvas = document.getElementById("canvas");

export async function loadModel() {
  // Create a detector.
  const solutionPath = "https://cdn.jsdelivr.net/npm/@mediapipe/pose";

  const detectorConfig = {
    runtime: "mediapipe",
    modelType: "full",
    solutionPath,
  };

  try {
    detector = await poseDetection.createDetector(
      poseDetection.SupportedModels.BlazePose,
      detectorConfig
    );
    console.log("model loaded!");
  } catch (err) {
    throw err;
  }

  // // const video = document.getElementById("video");
  // const videoElement = document.getElementsByClassName("input_video")[0];
  // const poses = await detector.estimatePoses(videoElement);

  // const camera = new Camera(videoElement, {
  //   onFrame: async () => {
  //     await poses.send({ image: videoElement });
  //   },
  //   width: 1280,
  //   height: 720,
  // });
  // camera.start();

  // console.log(poses[0].keypoints);
}

export async function loadVideo(options) {
  const video = await setupCamera(options);
  video.play();

  return video;
}

async function setupCamera(options) {
  if (!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia) {
    throw new Error(
      "Browser API navigator.mediaDevices.getUserMedia not available"
    );
  }

  let video = options.video;
  video.width = options.videoWidth;
  video.height = options.videoHeight;

  const mobile = isMobile();
  const stream = await navigator.mediaDevices.getUserMedia({
    audio: false,
    video: {
      facingMode: "user",
      width: mobile ? undefined : 640,
      height: mobile ? undefined : 480,
    },
  });
  video.srcObject = stream;

  return new Promise((resolve) => {
    video.onloadedmetadata = () => {
      resolve(video);
    };
  });
}

function isAndroid() {
  return /Android/i.test(navigator.userAgent);
}

//device ios?
function isiOS() {
  return /iPhone|iPad|iPod/i.test(navigator.userAgent);
}

//device mobile?
function isMobile() {
  return isAndroid() || isiOS();
}

export function detectPoseInRealTime(video, options) {
  //context canvas element on which to render the frames
  const canvas = options.canvas;
  const ctx = canvas.getContext("2d");
  ctx.fillStyle = "white";
  ctx.fillRect(0, 0, 150, 75);

  canvas.width = videoWidth;
  canvas.height = videoHeight;

  let angleScreenWidth = 0;

  // if (showAngles === true) {
  //   angleScreenWidth = 150;
  //   canvas.width += angleScreenWidth;
  // }

  // Because the image from camera is mirrored, need to flip horizontally.
  // ctx.translate(640, 0);
  // ctx.scale(-1, 1);
  // ctx.scale(-1, 1);
  // ctx.translate(-640, 0);

  async function poseDetectionFrame() {
    if (state.stopped === true) {
      ctx.clearRect(0, 0, videoWidth, videoHeight);
      ctx.save();
      // ctx.translate(videoWidth, 0);
      // ctx.scale(-1, 1);
      // ctx.scale(-1, 1);
      // ctx.translate(-videoWidth, 0);
      ctx.drawImage(video, 0, 0, videoWidth, videoHeight);
      ctx.restore();
    } else {
      //estimate the poses
      if (detector !== null) {
        //const imgData = ctx.getImageData(0, 0, videoWidth, videoHeight);
        const poses = await detector.estimatePoses(video, estimationConfig);
        //draw the frame on the canvas
        ctx.clearRect(0, 0, videoWidth, videoHeight);
        ctx.save();
        // ctx.translate(videoWidth, 0);
        // ctx.scale(-1, 1);
        ctx.scale(-1, 1);
        ctx.translate(-videoWidth, 0);
        ctx.drawImage(video, 0, 0, videoWidth, videoHeight);
        //ctx.restore();

        if (poses !== undefined && poses.length > 0) {
          //when model makes first estimation display the canvas and hide video
          if (displayVid === true) {
            //show the canvas once setup is done and hide the video.
            video.style.display = "none";
            canvas.style.display = "block";
            displayVid = false;
          }

          const { score, keypoints } = poses[0];

          addDerivedKeypoints(keypoints);

          //draw the keypoints on the canvas
          drawKeypoints(keypoints, ctx);
          //draw skeleton on the canvas
          drawSkeleton(keypoints, ctx);

          ctx.restore();

          // console.log(
          //   "Right Wrist x = " + keypoints[keypointsMapping.rightWrist].x
          // );
          // console.log(
          //   "Right Wrist y = " + keypoints[keypointsMapping.rightWrist].y
          // );
          // console.log(
          //   "Right Wrist z = " + keypoints[keypointsMapping.rightWrist].z
          // );
          // console.log(
          //   "Right Elbow x = " + keypoints[keypointsMapping.rightElbow].x
          // );
          // console.log(
          //   "Right Elbow y = " + keypoints[keypointsMapping.rightElbow].x
          // );
          // console.log(
          //   "Right Shoulder x = " + keypoints[keypointsMapping.rightShoulder].x
          // );
          // console.log(
          //   "Right Shoulder y = " + keypoints[keypointsMapping.rightShoulder].y
          // );

          detectKeypointsEvent.keypoints = keypoints;

          //Finding Angle
          findAngle(keypoints);
          console.log(state.angles);

          // checkAllKepointsVisible(keypoints);
          // console.log(state.allKeypointsVisible);

          detectAnglesEvent.angles = state.angles;

          // console.log(state.angles);
          ctx.font = "20px Arial";
          ctx.fillStyle = "red";
          // ctx.fillText(
          //   state.angles["rightKnee"] + "\xB0",
          //   videoWidth - keypoints[keypointsMapping.rightKnee].x,
          //   keypoints[keypointsMapping.rightKnee].y
          // );
          // ctx.fillText(
          //   state.angles["leftKnee"] + "\xB0  ",
          //   videoWidth - keypoints[keypointsMapping.leftKnee].x,
          //   keypoints[keypointsMapping.leftKnee].y
          // );
          // ctx.fillText(
          //   state.angles["rightHip"] + "\xB0",
          //   videoWidth - keypoints[keypointsMapping.rightHip].x,
          //   keypoints[keypointsMapping.rightHip].y
          // );
          // ctx.fillText(
          //   state.angles["leftElbow"] + "\xB0  ",
          //   videoWidth - keypoints[keypointsMapping.leftElbow].x,
          //   keypoints[keypointsMapping.leftElbow].y
          // );
          // ctx.fillText(
          //   state.angles["rightElbow"] + "\xB0",
          //   videoWidth - keypoints[keypointsMapping.rightElbow].x,
          //   keypoints[keypointsMapping.rightElbow].y
          // );
          // // ctx.fillText(
          // //   state.angles["leftHip"] + "\xB0  ",
          // //   videoWidth - keypoints[keypointsMapping.leftHip].x,
          // //   keypoints[keypointsMapping.leftHip].y
          // // );
          // ctx.fillText(
          //   state.angles["pelvic"] + "\xB0  ",
          //   videoWidth - keypoints[keypointsMapping.pelvic].x,
          //   keypoints[keypointsMapping.pelvic].y
          // );

          //}
        } else {
          ctx.restore();
        }
      }
    }
    //call poseDetectionFrame repeatedly.
    requestAnimationFrame(poseDetectionFrame);
  }

  poseDetectionFrame();
}

export const detectKeypointsEvent = {
  keypointsObj: {},
  defaultListener: function (keypoints) {},
  set keypoints(keypoints) {
    this.keypointsObj = keypoints;
    this.defaultListener(keypoints);
  },
};

export const detectAnglesEvent = {
  anglesObj: {},
  defaultListener: function (angles) {},
  set angles(angles) {
    this.anglesObj = angles;
    this.defaultListener(angles);
  },
};

navigator.getUserMedia =
  navigator.getUserMedia ||
  navigator.webkitGetUserMedia ||
  navigator.mozGetUserMedia;
// loadUtilities();
