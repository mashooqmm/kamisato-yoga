const initialState = {
  paused: false,
  stopped: false,
  angles: {
    leftShoulder: 0,
    rightShoulder: 0,
    leftElbow: 0,
    rightElbow: 0,
    leftHip: 0,
    rightHip: 0,
    leftKnee: 0,
    rightKnee: 0,
    pelvic: 0,
  },
  keypoints: {},
  skeletonColor: "red",
  allKeypointsVisible: false,
  timerStarted: false,
  asanaChanged: true,
  voice: "",
  asanaComplete: false,
  asana: "",
  lang: "",
};

let state = { ...initialState };

export { state, initialState };
