import { state } from "./state";
import { keypointsMapping } from "./mapping";
import {
  resetTimer,
  startTimer,
  speak,
  voiceCommand,
  voiceCommand1,
} from "./utils";

export async function vrksasana(keypoints) {
  if (state.asanaChanged) {
    state.asanaChanged = false;
    await voiceCommand(
      "Bend your left knee, and bring the sole of your left foot, high onto your inner right thigh, while keeping your shoulder down, so that both your palms touch each other."
    );
  }
  if (
    state.angles.leftKnee < 45 &&
    state.angles.leftKnee > 30 &&
    state.angles.leftElbow < 45 &&
    state.angles.leftElbow > 30 &&
    state.angles.rightElbow < 45 &&
    state.angles.rightElbow > 30 &&
    state.angles.pelvic < 100
  ) {
    state.skeletonColor = "lightgreen";
    if (!state.timerStarted) {
      state.timerStarted = true;
      startTimer(5);
    }
  } else if (state.angles.pelvic > 100) {
    await voiceCommand1("Please Stand up");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftKnee > 45) {
    await voiceCommand1("Bend your knee a bit more.");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftKnee < 30) {
    await voiceCommand1("Straighten your knee a bit more.");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftElbow > 45) {
    await voiceCommand1("Bend your Left Elbow a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.rightElbow > 45) {
    await voiceCommand1("Bend your Right Elbow a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftElbow < 30) {
    await voiceCommand1("Straighten your Left Elbow a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.rightElbow < 30) {
    await voiceCommand1("Straighten your Right Elbow a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else {
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  }
}

export async function balasana(keypoints) {
  if (state.asanaChanged) {
    state.asanaChanged = false;
    await voiceCommand(
      "Sit on your legs, now bend on your belly and extend your hands."
    );
  }
  if (
    state.angles.leftKnee < 40 &&
    state.angles.leftKnee > 15 &&
    state.angles.leftHip < 40 &&
    state.angles.leftHip > 20 &&
    state.angles.leftShoulder < 160 &&
    state.angles.leftShoulder > 125
  ) {
    state.skeletonColor = "lightgreen";
    if (!state.timerStarted) {
      state.timerStarted = true;
      startTimer(5);
    }
  } else if (state.angles.leftKnee > 40) {
    await voiceCommand1("Bend your knee a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftKnee < 15) {
    await voiceCommand1("Straighten your knee a bit");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftHip > 40) {
    await voiceCommand1("Bend your Hip a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftHip < 20) {
    await voiceCommand1("Straighten your Hip a bit");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftShoulder > 160) {
    await voiceCommand1("Bend your left Shoulder a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftHip < 125) {
    await voiceCommand1("Straighten your left Shoulder a bit");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else {
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  }
  resetTimer();
  state.timerStarted = false;
}

export async function padmasana(keypoints) {
  if (state.asanaChanged) {
    state.asanaChanged = false;
    await voiceCommand(
      "Cross your legs and sit, with your palms resting on your knees!"
    );
  }
  if (
    state.angles.leftKnee < 40 &&
    state.angles.leftKnee > 15 &&
    state.angles.leftShoulder < 40 &&
    state.angles.leftShoulder > 20
  ) {
    state.skeletonColor = "lightgreen";
    if (!state.timerStarted) {
      state.timerStarted = true;
      startTimer(5);
    }
  } else if (state.angles.leftKnee > 40) {
    await voiceCommand1("Bend your left knee a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftKnee < 15) {
    await voiceCommand1("Straighten your left knee a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftShoulder > 40) {
    await voiceCommand1("Bend your left shoulder a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftShoulder < 20) {
    await voiceCommand1("Straighten your left shoulder a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else {
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  }
}

export async function matsyasana(keypoints) {
  if (state.asanaChanged) {
    state.asanaChanged = false;
    voiceCommand(
      "Cross your legs and sit; then, with your legs in place, fall back and rest your body on the floor with your head leaned back; finally, with your hands, hold your toes and maintain the position."
    );
  }
  if (
    state.angles.leftKnee < 40 &&
    state.angles.leftKnee > 15 &&
    state.angles.leftHip < 170 &&
    state.angles.leftHip > 150 &&
    state.angles.leftElbow > 110 &&
    state.angles.leftElbow < 140
  ) {
    state.skeletonColor = "lightgreen";
    if (!state.timerStarted) {
      state.timerStarted = true;
      startTimer(5);
    }
  } else if (state.angles.leftKnee > 40) {
    await voiceCommand1("Bend your knee a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftKnee < 15) {
    await voiceCommand1("Straighten your knee a bit");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftHip > 170) {
    await voiceCommand1("Bend your hip a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftHip < 150) {
    await voiceCommand1("Straighten your hip a bit");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftElbow < 110) {
    await voiceCommand1("Straighten your elbow a bit");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftElbow > 140) {
    await voiceCommand1("Bend your elbow a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else {
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  }
  resetTimer();
  state.timerStarted = false;
}

export async function trikonsasana(keypoints) {
  if (state.asanaChanged) {
    state.asanaChanged = false;
    voiceCommand("Now Start doing trikonsasna");
  }
  if (
    state.angles.leftShoulder > 70 &&
    state.angles.leftShoulder < 110 &&
    state.angles.rightShoulder > 70 &&
    state.angles.rightShoulder < 110 &&
    state.angles.leftHip < 80 &&
    state.angles.leftHip > 40
  ) {
    state.skeletonColor = "lightgreen";
    if (!state.timerStarted) {
      state.timerStarted = true;
      startTimer(5);
    }
  } else if (state.angles.leftShoulder < 70) {
    await voiceCommand1("Straighten your left shoulder a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftShoulder > 110) {
    await voiceCommand1("Bend your left shoulder a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.rightShoulder < 70) {
    await voiceCommand1("Straighten your right shoulder a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.rightShoulder > 110) {
    await voiceCommand1("Bend your right shoulder a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftHip > 80) {
    await voiceCommand1("Bend your left hip a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftHip < 40) {
    await voiceCommand1("Straighten your left hip a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else {
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  }
}

export async function pranamasana(keypoints) {
  if (state.asanaChanged) {
    state.asanaChanged = false;
    await voiceCommand(
      "Bring the two hands together, and the palms touching each other at the chest level. Look straight ahead."
    );
  }
  if (
    state.angles.leftElbow > 30 &&
    state.angles.leftElbow < 45 &&
    state.angles.rightElbow > 30 &&
    state.angles.rightElbow < 45 &&
    state.angles.pelvic < 100
  ) {
    state.skeletonColor = "lightgreen";
    if (!state.timerStarted) {
      state.timerStarted = true;
      startTimer(5);
    }
  } else if (state.angles.pelvic > 100) {
    await voiceCommand1("Please Stand up");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftElbow < 30) {
    await voiceCommand1("Straighten your left elbow a bit");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftElbow > 45) {
    await voiceCommand1("Bend your left elbow a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.rightElbow < 30) {
    await voiceCommand1("Straighten your right elbow a bit");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.rightElbow > 45) {
    await voiceCommand1("Bend your right elbow a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else {
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  }
}

export async function hastaUttanasana(keypoints) {
  if (state.asanaChanged) {
    state.asanaChanged = false;
    voiceCommand("Now Start doing hastauttansana");
  }
  if (
    state.angles.leftShoulder > 150 &&
    state.angles.leftShoulder < 180 &&
    state.angles.rightShoulder > 150 &&
    state.angles.rightShoulder < 180 &&
    state.angles.leftElbow > 140 &&
    state.angles.leftElbow < 180 &&
    state.angles.rightElbow > 140 &&
    state.angles.rightElbow < 180 &&
    state.angles.leftHip > 120 &&
    state.angles.leftHip < 165
  ) {
    state.skeletonColor = "lightgreen";
    if (!state.timerStarted) {
      state.timerStarted = true;
      startTimer(2);
    }
  } else if (state.angles.leftShoulder < 150) {
    await voiceCommand1("Straighten your left shoulder a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftShoulder > 180) {
    await voiceCommand1("Bend your left shoulder a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.rightShoulder < 150) {
    await voiceCommand1("Straighten your right shoulder a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.rightShoulder > 180) {
    await voiceCommand1("Bend your right shoulder a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftElbow < 140) {
    await voiceCommand1("Straighten your left elbow a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftElbow > 180) {
    await voiceCommand1("Bend your left elbow a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.rightElbow < 140) {
    await voiceCommand1("Straighten your right elbow a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.rightElbow > 180) {
    await voiceCommand1("Bend your right elbow a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftHip < 120) {
    await voiceCommand1("Straighten your left hip a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftHip > 165) {
    await voiceCommand1("Bend your left hip a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else {
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  }
}

export async function hastaPadasana(keypoints) {
  if (state.asanaChanged) {
    state.asanaChanged = false;
    voiceCommand("Now Start doing hastaPadasana");
  }
  if (state.angles.leftHip > 30 && state.angles.leftHip < 60) {
    state.skeletonColor = "lightgreen";
    if (!state.timerStarted) {
      state.timerStarted = true;
      startTimer(5);
    }
  } else if (state.angles.leftHip < 30) {
    await voiceCommand1("Straighten your hip a bit");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftHip > 60) {
    await voiceCommand1("Bend your hip a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  }
}

export async function ashwaSanchalanasana(keypoints) {
  if (state.asanaChanged) {
    state.asanaChanged = false;
    voiceCommand("Now Start doing ashwasanchalanasana");
  }
  if (
    state.angles.leftHip > 20 &&
    state.angles.leftHip < 30 &&
    state.angles.leftKnee > 60 &&
    state.angles.leftKnee < 90 &&
    state.angles.rightKnee > 130 &&
    state.angles.rightKnee < 150
  ) {
    state.skeletonColor = "lightgreen";
    if (!state.timerStarted) {
      state.timerStarted = true;
      startTimer(5);
    }
  } else if (state.angles.leftHip < 20) {
    await voiceCommand1("Straighten your left hip a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftHip > 30) {
    await voiceCommand1("Bend your left hip a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftKnee < 60) {
    await voiceCommand1("Straighten your left knee a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftKnee > 90) {
    await voiceCommand1("Bend your left knee a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.rightKnee < 130) {
    await voiceCommand1("Straighten your right knee a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.rightKnee > 150) {
    await voiceCommand1("Bend your right knee a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else {
    await voiceCommand1("Bend your knee a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  }
}

export async function chaturangaDandasana(keypoints) {
  if (state.asanaChanged) {
    state.asanaChanged = false;
    voiceCommand("Now Start doing chaturangadandasana");
  }
  if (
    state.angles.leftElbow > 150 &&
    state.angles.leftElbow < 180 &&
    // state.angles.rightElbow > 160 &&
    // state.angles.rightElbow < 180 &&
    state.angles.leftHip > 160 &&
    state.angles.leftHip < 180 &&
    state.angles.leftShoulder < 100 &&
    state.angles.leftShoulder > 70
    // state.angles.rightShoulder < 100 &&
    // state.angles.rightShoulder > 70
  ) {
    state.skeletonColor = "lightgreen";
    if (!state.timerStarted) {
      state.timerStarted = true;
      startTimer(2);
    }
  } else if (state.angles.leftElbow < 160) {
    await voiceCommand1("Straighten your left elbow a bit");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftElbow > 180) {
    await voiceCommand1("Bend your left elbow a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
    // } else if (state.angles.rightElbow < 160) {
    //   await voiceCommand1("Straighten your right elbow a bit more");
    //   resetTimer();
    //   state.timerStarted = false;
    //   state.poseSuccesfull = false;
    // } else if (state.angles.rightElbow > 180) {
    //   await voiceCommand1("Bend your right elbow a bit more");
    //   resetTimer();
    //   state.timerStarted = false;
    //   state.poseSuccesfull = false;
  } else if (state.angles.leftHip < 160) {
    await voiceCommand1("Straighten your left hip a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftHip > 180) {
    await voiceCommand1("Bend your left hip a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
    state.angles.leftKnee < 190 && state.angles.leftKnee > 160;
  } else if (state.angles.leftShoulder > 100) {
    await voiceCommand1("Bend your left shoulder a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftShoulder < 70) {
    await voiceCommand1("Straighten your left shoulder a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
    // } else if (state.angles.rightShoulder > 100) {
    //   await voiceCommand1("Bend your right shoulder a bit more");
    //   resetTimer();
    //   state.timerStarted = false;
    //   state.poseSuccesfull = false;
    // } else if (state.angles.rightShoulder < 70) {
    //   await voiceCommand1("Straighten your right elbow a bit more");
    //   resetTimer();
    //   state.timerStarted = false;
    //   state.poseSuccesfull = false;
  }
}

export async function bhujangasana(keypoints) {
  if (state.asanaChanged) {
    state.asanaChanged = false;
    voiceCommand("Now Start doing bhujangasana");
  }
  if (
    state.angles.leftHip > 120 &&
    state.angles.leftHip < 140 &&
    state.angles.leftElbow > 160 &&
    state.angles.leftElbow < 180 &&
    state.angles.leftKnee > 160 &&
    state.angles.leftKnee < 180
  ) {
    state.skeletonColor = "lightgreen";
    if (!state.timerStarted) {
      state.timerStarted = true;
      startTimer(5);
    }
  } else if (state.angles.leftHip < 120) {
    await voiceCommand1("Straighten your hip a bit");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftHip > 140) {
    await voiceCommand1("Bend your hip a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftElbow < 160) {
    await voiceCommand1("Straighten your elbow a bit");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftElbow > 180) {
    await voiceCommand1("Bend your elbow a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftKnee < 160) {
    await voiceCommand1("Straighten your knee a bit");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftKnee > 180) {
    await voiceCommand1("Bend your knee a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else {
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  }
}

export async function adhoMukhaSvanasana(keypoints) {
  if (state.asanaChanged) {
    state.asanaChanged = false;
    voiceCommand("Now Start doing adhoMukhasvanasana");
  }
  if (
    state.angles.leftHip > 60 &&
    state.angles.leftHip < 100 &&
    state.angles.leftKnee < 190 &&
    state.angles.leftKnee > 160 &&
    state.angles.leftElbow < 190 &&
    state.angles.leftElbow > 160
  ) {
    state.skeletonColor = "lightgreen";
    if (!state.timerStarted) {
      state.timerStarted = true;
      startTimer(5);
    }
  } else if (state.angles.leftHip < 70) {
    await voiceCommand1("Straighten your hip a bit");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftHip > 80) {
    await voiceCommand1("Bend your hip a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else {
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  }
}

export async function ashtangaNamaskar(keypoints) {
  if (state.asanaChanged) {
    state.asanaChanged = false;
    voiceCommand("Now Start doing ashtanganamaskar");
  }
  if (
    state.angles.leftKnee > 100 &&
    state.angles.leftKnee < 160 &&
    state.angles.leftHip > 100 &&
    state.angles.leftHip < 160 &&
    state.angles.leftElbow > 20 &&
    state.angles.leftElbow < 50
  ) {
    state.skeletonColor = "lightgreen";
    if (!state.timerStarted) {
      state.timerStarted = true;
      startTimer(3);
    }
  } else if (state.angles.leftKnee < 100) {
    await voiceCommand1("Straighten your knee a bit");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftKnee > 160) {
    await voiceCommand1("Bend your knee a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftHip < 100) {
    await voiceCommand1("Straighten your hip a bit");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftHip > 160) {
    await voiceCommand1("Bend your hip a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftElbow < 20) {
    await voiceCommand1("Straighten your elbow a bit");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else if (state.angles.leftElbow > 50) {
    await voiceCommand1("Bend your elbow a bit more");
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  } else {
    resetTimer();
    state.timerStarted = false;
    state.poseSuccesfull = false;
  }
}

const steps = {
  pranamasana: true,
  hastaUttanasana: false,
  hastaPadasana: false,
  ashwaSanchalanasana: false,
  chaturangaDandasana: false,
  ashtangaNamaskar: false,
  bhujangasana: false,
  adhoMukhaSvanasana: false,
  ashwaSanchalanasana2: false,
  hastaPadasana2: false,
  hastaUttanasana2: false,
  pranamasana2: false,
};

export function suryanamaskar(keypoints) {
  if (steps.pranamasana2) {
    state.asana = "";
  } else {
    state.asana = "suryanamaskar";
  }
  if (state.asanaChanged) {
    state.asanaChanged = false;
    voiceCommand("Now Start doing Suryanamaskar");
  }

  if (steps.pranamasana == true) {
    if (state.asanaComplete) {
      voiceCommand("Now move to Hasta Uttanasana");
      state.asanaComplete = false;
      steps.pranamasana = false;
      steps.hastaUttanasana = true;
    }
    pranamasana(keypoints);
  } else if (steps.hastaUttanasana == true) {
    if (state.asanaComplete) {
      voiceCommand("Now Move to Hasta Padasana");
      state.asanaComplete = false;
      steps.hastaUttanasana = false;
      steps.hastaPadasana = true;
    }
    hastaUttanasana(keypoints);
  } else if (steps.hastaPadasana == true) {
    if (state.asanaComplete) {
      voiceCommand("Now Move to the Ashwa Sanchalasana");
      state.asanaComplete = false;
      steps.hastaPadasana = false;
      steps.ashwaSanchalanasana = true;
    }
    hastaPadasana(keypoints);
  } else if (steps.ashwaSanchalanasana == true) {
    if (state.asanaComplete) {
      voiceCommand("Now Move to Chaturanga Dandasana");
      state.asanaComplete = false;
      steps.ashwaSanchalanasana = false;
      steps.chaturangaDandasana = true;
    }
    ashwaSanchalanasana(keypoints);
  } else if (steps.chaturangaDandasana == true) {
    if (state.asanaComplete) {
      voiceCommand("Now Move to Ashtanga Namaskar");
      state.asanaComplete = false;
      steps.chaturangaDandasana = false;
      steps.ashtangaNamaskar = true;
    }
    chaturangaDandasana(keypoints);
  } else if (steps.ashtangaNamaskar == true) {
    if (state.asanaComplete) {
      voiceCommand("Now Move to Bhujangasana");
      state.asanaComplete = false;
      steps.ashtangaNamaskar = false;
      steps.bhujangasana = true;
    }
    ashtangaNamaskar(keypoints);
  } else if (steps.bhujangasana == true) {
    if (state.asanaComplete) {
      voiceCommand("Now Move to Adho Mukha Svanasana");
      state.asanaComplete = false;
      steps.bhujangasana = false;
      steps.adhoMukhaSvanasana = true;
    }
    bhujangasana(keypoints);
  } else if (steps.adhoMukhaSvanasana == true) {
    if (state.asanaComplete) {
      voiceCommand("Now Move to Ashwa Sanchalasana");
      state.asanaComplete = false;
      steps.adhoMukhaSvanasana = false;
      steps.ashwaSanchalanasana2 = true;
    }
    adhoMukhaSvanasana(keypoints);
  } else if (steps.ashwaSanchalanasana2 == true) {
    if (state.asanaComplete) {
      voiceCommand("Now Move to Hasta Padasana");
      state.asanaComplete = false;
      steps.ashwaSanchalanasana2 = false;
      steps.hastaPadasana2 = true;
    }
    ashwaSanchalanasana(keypoints);
  } else if (steps.hastaPadasana2 == true) {
    if (state.asanaComplete) {
      voiceCommand("Now Move to Hasta Uttasana");
      state.asanaComplete = false;
      steps.hastaPadasana2 = false;
      steps.hastaUttanasana2 = true;
    }
    hastaPadasana(keypoints);
  } else if (steps.hastaUttanasana2 == true) {
    if (state.asanaComplete) {
      voiceCommand("Now Move to Pramanasana");
      state.asanaComplete = false;
      steps.hastaUttanasana2 = false;
      steps.pranamasana2 = true;
    }
    hastaUttanasana(keypoints);
  } else if (steps.pranamasana2 == true) {
    if (state.asanaComplete) {
      voiceCommand("Rejoice! You've Completed Suryanamaskar!");
      state.asanaComplete = false;
      steps.pranamasana2 = false;
      steps.pranamasana = true;
    }
    pranamasana(keypoints);
  }
}
