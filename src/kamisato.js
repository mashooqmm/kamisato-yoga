import { detectAnglesEvent, detectKeypointsEvent } from "./modelConfig";
import {
  checkAllKepointsVisible,
  voiceCommand,
  voiceCommand1,
  voiceCancel,
  translate,
} from "./utils";
import { loadModel, loadVideo, detectPoseInRealTime } from "./modelConfig";
import { state, initialState } from "./state";
import translateMin from "translate";
import { throttle } from "lodash";
import {
  adhoMukhaSvanasana,
  ashtangaNamaskar,
  ashwaSanchalanasana,
  balasana,
  bhujangasana,
  chaturangaDandasana,
  hastaPadasana,
  hastaUttanasana,
  padmasana,
  pranamasana,
  suryanamaskar,
  trikonsasana,
  vrksasana,
} from "./asanas";

// const videoElement = document.getElementsByClassName("input_video")[0];
// const canvasElement = document.getElementsByClassName("output_canvas")[0];
// const canvasCtx = canvasElement.getContext("2d");
// const landmarkContainer = document.getElementsByClassName(
//   "landmark-grid-container"
// )[0];
// const grid = new LandmarkGrid(landmarkContainer);

async function loadUtilities(options) {
  //video from webcam
  await loadModel();

  let video;

  try {
    video = await loadVideo(options);
  } catch (e) {
    alert(
      "this browser does not support video capture," +
        "or this device does not have a camera"
    );
    throw e;
  }

  //Start detecting the poses
  detectPoseInRealTime(video, options);
}

export async function start(options) {
  await loadUtilities(options);
}

export function stop() {
  // //clear the angles displayed
  // const ctx = canvas.getContext("2d");
  // ctx.clearRect(videoWidth, 0, 150, videoHeight);
  // //reset state to initial state
  // //change state back to initial state
  // for (const [key, value] of Object.entries(initialState)) {
  //   state[key] = value;
  // }
  state.stopped = true;
}

export function restart() {
  state.stopped = false;
}

export function keypointsListener(callback) {
  detectKeypointsEvent.defaultListener = callback;
}

export function anglesListener(callback) {
  detectAnglesEvent.defaultListener = callback;
}

export function checkAsana(asana) {
  state.asanaChanged = true;
  state.asana = asana;
  detectKeypointsEvent.defaultListener = function (keypoints) {
    checkAllKepointsVisible(keypoints);
    if (state.allKeypointsVisible) {
      // voiceCancel();
      if (asana == "matsyasana") {
        matsyasana(keypoints);
      } else if (asana == "padmasana") {
        padmasana(keypoints);
      } else if (asana == "balasana") {
        balasana(keypoints);
      } else if (asana == "trikonsasana") {
        trikonsasana(keypoints);
      } else if (asana == "vrksasana") {
        vrksasana(keypoints);
      } else if (asana == "pranamasana") {
        pranamasana(keypoints);
      } else if (asana == "hastaPadasana") {
        hastaPadasana(keypoints);
      } else if (asana == "hastaUttanasana") {
        hastaUttanasana(keypoints);
      } else if (asana == "ashwaSanchalanasana") {
        ashwaSanchalanasana(keypoints);
      } else if (asana == "chaturangaDandasana") {
        chaturangaDandasana(keypoints);
      } else if (asana == "ashtangaNamaskar") {
        ashtangaNamaskar(keypoints);
      } else if (asana == "bhujangasana") {
        bhujangasana(keypoints);
      } else if (asana == "adhoMukhaSvanasana") {
        adhoMukhaSvanasana(keypoints);
      } else if (asana == "suryanamaskar") {
        suryanamaskar(keypoints);
      }
    } else {
      voiceCommand1("Please move back!");
    }
  };
}
