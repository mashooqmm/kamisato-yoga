import { keypointsMapping, angleJoints, angleInd } from "./mapping";
import { state } from "./state";
import { reject, throttle } from "lodash";
import translateMin from "translate";

var scoreThreshold = 0.7;

export function checkAllKepointsVisible(keypoints) {
  console.log(state.asana);
  let leftShoulderScore = keypoints[keypointsMapping.leftShoulder].score;
  let rightShoulderScore = keypoints[keypointsMapping.rightShoulder].score;
  let leftAnkleScore = keypoints[keypointsMapping.leftFootIndex].score;
  let rightAnkleScore = keypoints[keypointsMapping.rightFootIndex].score;

  const scores = [
    leftShoulderScore,
    rightShoulderScore,
    leftAnkleScore,
    rightAnkleScore,
  ];
  if (
    (leftShoulderScore > scoreThreshold &&
      rightShoulderScore > scoreThreshold &&
      (leftAnkleScore > scoreThreshold || rightAnkleScore > scoreThreshold)) ||
    state.asana == "padmasana"
  ) {
    // voiceCancel();
    state.skeletonColor = "white";
    state.allKeypointsVisible = true;
  } else {
    state.skeletonColor = "red";
    state.allKeypointsVisible = false;
    resetTimer();
    // voiceCommand1("Please Move Back");
  }
}

export function findAngle(keypoints) {
  for (let i = 0; i < angleJoints.length; i++) {
    // const m1 =
    //   (keypoints[14].y - keypoints[16].y) / (keypoints[14].x - keypoints[16].x);
    // const m2 =
    //   (keypoints[12].y - keypoints[14].y) / (keypoints[12].x - keypoints[14].x);

    // console.log(m1, m2);

    // const rad = Math.atan((m2 - m1) / (1 + m1 * m2));
    // const angle = Math.abs((rad * 180.0) / Math.PI);
    // if (angle > 90.0) {
    //   angle = 360 - angle;
    // }

    var P01 = Math.sqrt(
      Math.pow(
        keypoints[angleJoints[i][1]].x - keypoints[angleJoints[i][0]].x,
        2
      ) +
        Math.pow(
          keypoints[angleJoints[i][1]].y - keypoints[angleJoints[i][0]].y,
          2
        )
    );
    var P12 = Math.sqrt(
      Math.pow(
        keypoints[angleJoints[i][1]].x - keypoints[angleJoints[i][2]].x,
        2
      ) +
        Math.pow(
          keypoints[angleJoints[i][1]].y - keypoints[angleJoints[i][2]].y,
          2
        )
    );
    var P02 = Math.sqrt(
      Math.pow(
        keypoints[angleJoints[i][2]].x - keypoints[angleJoints[i][0]].x,
        2
      ) +
        Math.pow(
          keypoints[angleJoints[i][2]].y - keypoints[angleJoints[i][0]].y,
          2
        )
    );
    var rad = Math.acos((P12 * P12 + P01 * P01 - P02 * P02) / (2 * P12 * P01));
    // var rad =
    //   Math.atan2(
    //     keypoints[angleJoints[i][2]].y - keypoints[angleJoints[i][1]].y,
    //     keypoints[angleJoints[i][2]].x - keypoints[angleJoints[i][1]].x
    //   ) -
    //   Math.atan2(
    //     keypoints[angleJoints[i][0]].y - keypoints[angleJoints[i][1]].y,
    //     keypoints[angleJoints[i][0]].x - keypoints[angleJoints[i][1]].x
    //   );
    var angle = Math.abs((rad * 180.0) / Math.PI);

    // if (angle >= 180.0) {
    //   angle = 180.0 - angle;
    // }

    angle = Math.round(angle);

    state.angles[angleInd[i]] = angle;
  }
}

var timer;
var interval;
export function startTimer(duration) {
  console.log(state.asana);
  console.log(state.asanaComplete);
  console.log(state.timerStarted);
  var display = document.querySelector("#time");
  timer = duration;
  var minutes, seconds;
  interval = setInterval(function () {
    minutes = parseInt(timer / 60, 10);
    seconds = parseInt(timer % 60, 10);

    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;

    display.textContent = minutes + ":" + seconds;

    if (--timer < 0) {
      if (state.asana != "suryanamaskar") {
        voiceCommand("Succesfully completed!");
        clearInterval(interval);
      } else {
        state.asanaComplete = true;
        clearInterval(interval);
      }
    }
  }, 1000);
}

export function resetTimer() {
  clearInterval(interval);
}

var synth = speechSynthesis;

// function voiceChanger() {
//   var synth = speechSynthesis;
//   var voices;
//   if (synth.onvoiceschanged !== undefined) {
//     console.log(synth.onvoiceschanged);
//     synth.onvoiceschanged = () => populateVoiceList();
//   }
//   function populateVoiceList() {
//     voices = synth.getVoices();
//     state.voices = voices[state.voice];
//     console.log(state.voices);
//   }
// }

export function voiceCommand(speech) {
  translateMin(speech, { to: state.lang }).then((res) => {
    var synth1 = speechSynthesis;
    var voices = synth1.getVoices();
    var utterThis = new SpeechSynthesisUtterance(res);
    utterThis.voice = voices[state.voice];
    utterThis.pitch = 1;
    utterThis.rate = 0.9;
    synth1.speak(utterThis);
    return new Promise((resolve, reject) => {
      utterThis.onend = resolve;
    });
  });
}

export const voiceCommand1 = throttle(function (speech) {
  translateMin(speech, { to: state.lang }).then((res) => {
    var voices = synth.getVoices();
    console.log(voices);
    var utterThis = new SpeechSynthesisUtterance(res);
    utterThis.voice = voices[state.voice];
    utterThis.pitch = 1;
    utterThis.rate = 0.9;
    synth.speak(utterThis);
  });
}, 10000);

export function voiceCancel() {
  synth.cancel();
}

var langCodes = {
  tamil: "ta",
  english: "en",
  hindi: "hi",
  french: "fr",
};

var voiceCodes = {
  tamil: 53,
  english: 8,
  hindi: 55,
  french: 54,
};

export function setVoice(lang) {
  state.lang = langCodes[lang];
  state.voice = voiceCodes[lang];
}

export function changeVoice(lang) {
  state.lang = langCodes[lang];
  state.voice = voiceCodes[lang];
}

export const translate = throttle(function (speech) {
  translateMin(speech, { to: state.lang }).then((res) => {
    responsiveVoice.speak(res, state.voices);
  });
}, 10000);

export function speak(speech) {
  translateMin(speech, { to: state.lang }).then((res) => {
    return new Promise((resolve, reject) => {
      responsiveVoice.speak(res, state.voices, { onend: resolve });
    });
  });
}
