import { util, SupportedModels } from "@tensorflow-models/pose-detection";
import { state } from "./state";
import { keypointsMapping } from "./mapping";

const modelName = SupportedModels.BlazePose;
const scoreThreshold = 0.7;

export function drawKeypoints(keypoints, ctx) {
  const keypointInd = util.getKeypointIndexBySide(modelName);

  keypointInd.middle.push(keypointsMapping.neck);
  keypointInd.middle.push(keypointsMapping.nose);
  //ctx.fillStyle = color;
  ctx.strokeStyle = state.skeletonColor;
  ctx.lineWidth = 2;

  // for(const keypoint of keypoints) {
  //   drawKeypoint(keypoint, ctx);
  // }

  for (const i of keypointInd.middle) {
    drawKeypoint(keypoints[i], ctx);
  }

  ctx.fillStyle = "Green";
  for (const i of keypointInd.left) {
    drawKeypoint(keypoints[i], ctx);
  }

  ctx.fillStyle = "Orange";
  for (const i of keypointInd.right) {
    drawKeypoint(keypoints[i], ctx);
  }
}

function drawKeypoint(keypoint, ctx) {
  const score = keypoint.score != null ? keypoint.score : 1;

  if (score >= scoreThreshold) {
    const circle = new Path2D();
    circle.arc(keypoint.x, keypoint.y, 2, 0, 2 * Math.PI);
    ctx.fill(circle);
    ctx.stroke(circle);

    //increase count for visible keypoints
    //Kcount++;
  }
}

export function drawSkeleton(keypoints, ctx) {
  ctx.fillStyle = state.skeletonColor;
  ctx.strokeStyle = state.skeletonColor;
  ctx.lineWidth = 2;

  const adjacentPairs = util.getAdjacentPairs(modelName);

  adjacentPairs.push([keypointsMapping.nose, keypointsMapping.neck]);
  adjacentPairs.push([keypointsMapping.neck, keypointsMapping.pelvic]);
  adjacentPairs.push([keypointsMapping.leftKnee, keypointsMapping.pelvic]);
  adjacentPairs.push([keypointsMapping.rightKnee, keypointsMapping.pelvic]);

  adjacentPairs.forEach(([i, j]) => {
    const kp1 = keypoints[i];
    const kp2 = keypoints[j];

    // if(kp1['name'] === 'leftShoulder' && kp2['name'] === 'leftHip' || kp1['name'] === 'rightShoulder' && kp2['name'] === 'rightHip') {
    //   return
    // }

    // If score is null, just show the keypoint.
    const score1 = kp1.score != null ? kp1.score : 1;
    const score2 = kp2.score != null ? kp2.score : 1;
    const scoreThreshold = 0.5;

    if (score1 >= scoreThreshold && score2 >= scoreThreshold) {
      ctx.beginPath();
      ctx.moveTo(kp1.x, kp1.y);
      ctx.lineTo(kp2.x, kp2.y);
      ctx.stroke();
    }
  });
}

export function addDerivedKeypoints(keypoints) {
  const lShoulder = keypoints[keypointsMapping.leftShoulder];
  const rShoulder = keypoints[keypointsMapping.rightShoulder];
  const lHip = keypoints[keypointsMapping.leftHip];
  const rHip = keypoints[keypointsMapping.rightHip];

  const { x: xShLeft, y: yShLeft, z: zShLeft } = lShoulder;
  const { x: xShRight, y: yShRight, z: zShRight } = rShoulder;

  let x = (xShLeft + xShRight) / 2;
  let y = (yShLeft + yShRight) / 2;
  let z = (zShLeft + zShRight) / 2;
  let midScore = Math.min(lShoulder.score, rShoulder.score);

  keypoints.push({
    x,
    y,
    z,
    name: "neck",
    score: midScore,
  });

  const { x: xHipLeft, y: yHipLeft, z: zHipLeft } = lHip;
  const { x: xHipRight, y: yHipRight, z: zHipRight } = rHip;

  x = (xHipLeft + xHipRight) / 2;
  y = (yHipLeft + yHipRight) / 2;
  z = (zHipLeft + zHipRight) / 2;
  midScore = Math.min(lHip.score, rHip.score);

  keypoints.push({
    x,
    y,
    z,
    name: "pelvic",
    score: midScore,
  });
}
