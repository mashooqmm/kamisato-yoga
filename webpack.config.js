const path = require("path");

const devConfig = {
  mode: "development",
  devtool: "eval-source-map",
  entry: "./src/index.js",
  output: {
    filename: "kamisato.js",
    path: path.resolve(__dirname, "dist"),
    library: {
      name: "kamisato",
      type: "umd",
      umdNamedDefine: true,
    },
  },

  devServer: {
    static: path.join(__dirname, "dist"),
    hot: true,
  },
};

const prodConfig = {
  mode: "production",
  entry: "./src/index.js",
  output: {
    filename: "kamisato.js",
    path: path.resolve(__dirname, "dist"),
    library: {
      name: "kamisato",
      type: "umd",
    },
  },
};

module.exports = (env) => {
  console.log(env.dev);
  if (env.dev) return devConfig;
  else return prodConfig;
};
